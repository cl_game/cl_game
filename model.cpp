#include "model.h"
#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <iostream>
#include <cassert>

using std::endl;
using std::cout;
using namespace clan;

Model::Model( GraphicContext & gci, const std::string &fn )  {
    
    // populate model data struct using Assimp
    data = std::make_shared<ModelData>( ModelData() );
    _loadData( fn );
    
    // store pointer to graphic context
    gc = &gci;
    
}


bool Model::UploadData( Store<Texture2D> & tstore ) {
    
    // ensure that textures are uploaded
    for( int i=0; i<data->textures.size(); ++i )
    {
        // get texture name
        std::string n = data->textures[i].name;
        
        // ensure presence in texture store
        tstore.Get( n );
    }

    // count total vertices in model
    int vertex_count = _countVertices();
    
    // reserve space for GPU buffers
    vbo_positions = VertexArrayVector<Vec3f>(*gc, vertex_count);
    vbo_normals = VertexArrayVector<Vec3f>(*gc, vertex_count);
    vbo_texcoords = VertexArrayVector<Vec2f>(*gc, vertex_count);
    
    // upload data for each mesh
    unsigned int size;
    unsigned int offset = 0;
    for (int i = 0; i < data->meshes.size(); ++i)
    {
        // get size of current mesh
        size = data->meshes[i].vertices.size();
        
        // upload positions
        vbo_positions.upload_data(*gc,offset,&data->meshes[i].vertices[0],size);
        
        // upload normals
        vbo_normals.upload_data(*gc,offset,&data->meshes[i].normals[0],size);
        
        // upload uv coords
        vbo_texcoords.upload_data(*gc,offset,&data->meshes[i].channels[0][0],size);
        
        offset += size;
    }
    
    return true;
}


void Model::Draw( GraphicContext & gc, GPass & gp, Mat4f & view, Mat4f & model )
{
    Mat4f MVP = *gp.GetProjectionMatrix() * view * model;
    
    PrimitivesArray prim_array(gc);
    
    gp.Use( gc, MVP, model );
    
    // draw primitives for each mesh
    unsigned int size;
    unsigned int offset = 0;
    for (int i = 0; i < data->meshes.size(); ++i)
    {
        // get size of current mesh
        size = data->meshes[i].vertices.size();
        
        // set attribute positions
        prim_array.set_attributes(0,vbo_positions,3,type_float,offset*3);
        prim_array.set_attributes(1,vbo_texcoords,2,type_float,offset*2);
        prim_array.set_attributes(2,vbo_normals,3,type_float,offset*3);
        
        // set texture
        std::string texture_name = data->textures[i].name;
        Texture2D tex = *(gp.textureStore->Get(texture_name));
        
        gc.set_texture(0,tex);
        
        // draw call
        gc.draw_primitives(type_triangles,size,prim_array);
            
        offset += size;
    }
    
}




bool Model::_loadData( const std::string &fn ) {

    // import file using Assimp
    const aiScene * scene = aiImportFile( fn.c_str(),
                        aiProcess_Triangulate|aiProcess_JoinIdenticalVertices );
    
    // return false if scene didn't load correctly
    if( !scene )
    {
        cout << "Error: couldn't load model file" << endl;
        return false;
    }

    // return false if there are no meshes
    if( !scene->HasMeshes() )
    {
        cout << "Error: no meshes in model file" << endl;
        return false;
    }
    
    // false if there are no normals
    if( !scene->mMeshes[0]->HasNormals() )
    {   
        cout << "Error: meshes lack normals" << endl;
        return false;
    }
    
    // allocate model data
    data->meshes.reserve(scene->mNumMeshes);
    
    // iterate over materials
    ModelDataTexture * tex_arr = new ModelDataTexture [scene->mNumMaterials];
    for (int i = 0; i<scene->mNumMaterials; ++i)
    {
        aiMaterial * aimat = scene->mMaterials[i];
        ModelDataTexture mdt = tex_arr[i];
        
        std::string * sName;

        // load textures
        if(aimat->GetTextureCount(aiTextureType_DIFFUSE) > 0)
        {        
            aiString * name = new aiString;
            aimat->GetTexture(aiTextureType_DIFFUSE,0,name);
            sName = new std::string(name->C_Str());
        }
        else
        {
            // name 'null' if no texture present in material
            sName = new std::string("null");
        }
        
        // store texture name in local array
        tex_arr[i].name = *sName;
    }

    
    // iterate over meshes
    for ( int i=0; i<scene->mNumMeshes; ++i )
    {
        // allocate new mesh data
        aiMesh * am = scene->mMeshes[i];
        ModelDataMesh * m = new ModelDataMesh();
        
        // set texture data from local array
        data->textures.push_back( tex_arr[am->mMaterialIndex] );
        
        // reserve data
        m->normals.reserve( am->mNumFaces * 3 );
        m->vertices.reserve( am->mNumFaces * 3 );
        m->channels.reserve(1); 
        // only the first UV channel is populated
        m->channels.push_back( std::vector<Vec2f>() );
        m->channels[0].reserve( am->mNumFaces * 3 );

        // iterate over faces
        for ( int j=0; j<am->mNumFaces; ++j )
        {
            // get pointer to ai face
            aiFace af = am->mFaces[j];
            
            // check for triangle faces
            assert( af.mNumIndices == 3 );
            
            // iterate over face indices
            for ( int k=0; k<3; ++k ) {
                m->vertices.push_back(Vec3f(&am->mVertices[af.mIndices[k]].x));
                m->normals.push_back(Vec3f(&am->mNormals[af.mIndices[k]].x));
                m->channels[0].push_back(Vec2f(&am->mTextureCoords[0][af.mIndices[k]].x));
            }
        }
        
        // add mesh to model data
        data->meshes.push_back(*m);
    }
    
    // release Assimp scene resource
    aiReleaseImport( scene );  
    
    return true;
}

int Model::_countVertices() {
    if( data == NULL )
        return 0;
    else {
        int count = 0;
        for( int i=0; i<data->meshes.size(); ++i )
            count += data->meshes[i].vertices.size();
        return count;
    }
}
