/* 
 * 
 * Class: Model
 * ============
 * Assumes 1 texture per mesh.
 * Assumes single hierarchy model, ie. no parent or child nodes.
 *
 */
#ifndef MODEL_H
#define MODEL_H

#include <ClanLib/core.h>
#include <ClanLib/gl.h>
#include <ClanLib/Scene3D/ModelData/model_data.h>
#include <ClanLib/Display/Render/texture_2d.h>
#include <string>
#include <vector>


#include "gpass.h"  // this didn't like to be loaded after store.hpp    ( ??? )
#include "store.hpp"


using namespace clan;


class Model {

    public:

        Model( GraphicContext & gc, const std::string &fn );
        bool UploadData( Store<Texture2D> & );
        void Draw( GraphicContext & gc, GPass & gp, Mat4f & view, Mat4f & model );

    private:

        std::shared_ptr<ModelData> data;
        GraphicContext * gc;

        VertexArrayVector<Vec3f> vbo_positions;
        VertexArrayVector<Vec3f> vbo_normals;
        VertexArrayVector<Vec2f> vbo_texcoords;

        bool _loadData( const std::string &fn );
        int _countVertices();

};


#endif // MODEL_H
