/* 
 * 
 * Class: Store
 * ============
 * Stores GraphicContex-related objects of template type.
 * Provides pointers.
 * Loads objects as needed into a single GraphicContext.
 *
 */

#ifndef STORE_HPP
#define STORE_HPP

#include <ClanLib/gl.h>
#include <map>
#include <string>
#include <iostream>

using namespace clan;
using std::cout;
using std::endl;

template<class StoreType>
class Store
{

    public:
        Store( GraphicContext * gc, const std::string &dir );
        StoreType * Get( const std::string &name );

        int num_items;

    private:
        GraphicContext * gc;
        std::map<std::string,StoreType*> StoreLib;
        std::string source_dir;       

};


/*
 * Default constructor
 */
template<class StoreType>
Store<StoreType>::Store( GraphicContext * gc_in, const std::string &dir )
{
    // store class attributes
    source_dir = dir;
    gc = gc_in;
    num_items = 0;
}


/*
 * Checks for pointer in StoreLib
 * Loads new StoreType as needed
 */
template<class StoreType>
StoreType * Store<StoreType>::Get( const std::string &name )
{
    StoreType * tmp;
    
    if( StoreLib[name] != NULL )
        tmp = StoreLib[name];
    else
    {
        try
        {
            tmp = new StoreType(*gc,source_dir+name);
        }
        catch(...)
        {
            cout << "Error: unable to load file "+source_dir+name << endl;
            exit(1);
        }
        StoreLib[name] = tmp;
        num_items += 1;
    }
    
    return tmp;
}



#endif // STORE_HPP
