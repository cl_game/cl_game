#include "gpass.h"



GPass::GPass( GraphicContext & gc, Mat4f &proj_in, Store<Texture2D> & tStore )
{    
    // read in vert and frag shaders
    std::string s = File::read_text("shaders/gpass.vert");
    ShaderObject vs(gc,shadertype_vertex,s);
    s = File::read_text("shaders/gpass.frag");
    ShaderObject fs(gc,shadertype_fragment,s);

    // compile vert and frag shaders
    if( !vs.compile() )
        throw Exception(string_format("Unable to compile vertex shader object: %1",vs.get_info_log()));
    
    if( !fs.compile() )
        throw Exception(string_format("Unable to compile fragment shader object: %1",fs.get_info_log()));
    
    // link program
    program = ProgramObject(gc);
    program.attach(vs);
    program.attach(fs);
    program.bind_attribute_location(0,"Position");
    program.bind_attribute_location(1,"TexCoord");
    program.bind_attribute_location(2,"Normal");
    program.bind_frag_data_location(0,"DiffuseOut");
    program.bind_frag_data_location(1,"WorldPosOut");
    program.bind_frag_data_location(2,"NormalOut");
    if (!program.link())
    {
        throw Exception(string_format("Unable to link program object: %1", program.get_info_log()));
    }
    
    // set projection pointer
    projection = &proj_in;
    
    // set texture store pointer
    textureStore = &tStore;

}

void GPass::Use( GraphicContext &gc,const Mat4f &MVP,const Mat4f &Model ) 
{
    program.set_uniform_matrix("gMVP",MVP);
    program.set_uniform_matrix("gModel",Model);
    
}


Mat4f * GPass::GetProjectionMatrix()
{
    return projection;
}
