#include "gbuffer.h"




GBuffer::GBuffer( GraphicContext &gc )
{
    // store context and generate framebuffer
    fb = FrameBuffer(gc);

    // generate render targets
    for(int i=0; i<3; ++i)
        targets[i] = Texture2D(gc,gc.get_size(),tf_rgba8);
    targets[3] = Texture2D(gc,gc.get_size(),tf_depth_component32f); // depth buffer

    // attach render targets to framebuffer
    fb.attach_color(0,targets[0]);  // diffuse
    fb.attach_color(1,targets[1]);  // position
    fb.attach_color(2,targets[2]);  // normals
    fb.attach_depth(targets[3]);    // depth
}



void GBuffer::BindForReading( GraphicContext & gc )
{
    gc.reset_frame_buffer();
    gc.set_frame_buffer(gc.get_write_frame_buffer(),fb);
}



void GBuffer::BindForWriting( GraphicContext & gc )
{
    gc.set_frame_buffer(fb);
    gc.clear();
}


Texture2D * GBuffer::GetBufferTexture( int num )
{
    return &targets[num];
}
