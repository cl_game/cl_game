#ifndef GPASS_H
#define GPASS_H

#include <ClanLib/core.h>
#include <ClanLib/display.h>
#include <iostream>

#include "store.hpp"

using namespace clan;
using std::cout;
using std::endl;

class GPass {

    public:
    
        GPass( GraphicContext &, Mat4f & proj_in, Store<Texture2D> & tStore );
        void Use( GraphicContext&,const Mat4f&,const Mat4f& );
        Mat4f * GetProjectionMatrix();
        
        
        Mat4f * projection;
        Store<Texture2D> * textureStore;
    
    private:
    
        ProgramObject program;

};


#endif // GPASS_H
