// header files
#include <cassert>
#include <ClanLib/core.h>
#include <ClanLib/display.h>
#include <ClanLib/gl.h>

#include "model.h"
#include "store.hpp"
#include "gbuffer.h"
#include "gpass.h"

// handle namespaces
using namespace clan;
using std::cout;
using std::endl;

// global constants
std::vector<std::string> args;  // stores arg values

struct body
{
    Model model;
    Mat4f transform;
};


// main program
int main(int argc,char ** argv)
{
    
    // store args
    for( int i=0; i<argc; ++i )
        args.push_back( std::string(argv[i]) );
    
    // print welcome msg
    cout << "====================================================" << endl;
    cout << "=== Starting Cool ClanLib Program Learning Suite ===" << endl;
    cout << "====================================================" << endl;

    // set up ClanLib stuff
    SetupCore setup_core;
    SetupDisplay setup_display;
    SetupGL setup_gl;

    // set up window and GL context
    DisplayWindow window("Hello World", 640, 480, false, true);
    GraphicContext gc = window.get_gc();

    // create resource managers
    Store<Texture2D> TextureStore(&gc,"textures/");
    Store<Model> ModelStore(&gc,"models/");

    // report info on GL context and system
    cout << "OpenGL version:\t\t\t" << gc.get_major_version()
        << ' ' << gc.get_minor_version() << endl;
    cout << "Shader language:\t\t" << gc.get_shader_language() << endl;
    cout << "Supports compute shader:\t"
                << gc.has_compute_shader_support() << endl;
    cout << "Max texture size:\t\t" << gc.get_max_texture_size() << endl;
    cout << "CPU cores:\t\t\t" << System::get_num_cores() << endl;
    
    // set up deferred rendering buffers
    GBuffer gb(gc);
    
    // view and projection matrices
    Mat4f view = Mat4f::look_at(Vec3f(1,2,1),     // eye_position
                                Vec3f(0,0,0),       // center_of_view
                                Vec3f(0,1,0) );     // up_direction
    Mat4f projection = Mat4f::perspective(  45.0f,  // fov_y_degrees
                                            float(gc.get_width())/float(gc.get_height()), // aspect_ratio
                                            0.0001f,    // z_near
                                            100.0f,     // z_far
                                            handed_right,
                                            clip_negative_positive_w );
    
    // set up geometry pass shaders
    GPass gp(gc,projection,TextureStore);
    
    // set up lighting pass shaders
    
    
    // do misc. stuff here
    Model * md = ModelStore.Get("object.dae");
    md -> UploadData(TextureStore);
    
    Mat4f model_pos = Mat4f::identity() * Mat4f::rotate(Angle::from_degrees(17),0,1,0);
    
    Texture2D * texp = TextureStore.Get("red_noise.png");
    

    Image img(*texp,Rect(0,0,texp->get_width(),texp->get_height()));
    img.set_linear_filter(false);
    img.set_scale(12,12);
    
    
    // prepare game loop
    window.hide_cursor();
    Canvas canvas(window);
    InputDevice keyboard = window.get_ic().get_keyboard();
    FontDescription fd;
    fd.set_height(64);
    fd.set_weight(20);
    clan::Font font(canvas,fd,"fonts/Cousine-Bold.ttf");
    
    // begin game loop
    while( !keyboard.get_keycode(keycode_escape) )
    {
        // Draw with the canvas:
        canvas.clear(Colorf::dimgrey);
        
        // draw stuff here
        font.draw_text(canvas,100,100,"Hello World!");
        img.draw(canvas,0,0);

        
        // start geometry pass
        gb.BindForWriting(gc);
        
        // render geometry
        md->Draw(gc,gp,view,model_pos);
        
        // start lighting pass
        gb.BindForReading(gc);

        // render lighting
        

        // Draw any remaining queued-up drawing commands on canvas:
        canvas.flush();

        // Present the frame buffer content to the user:
        window.flip();

        // Read any messages from the windowing system message queue
        KeepAlive::process();
        
    }

    // exit program
    return 0;
}
