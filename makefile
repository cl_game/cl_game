LIBS = -lclan30Core -lclan30Display -lclan30App -lclan30GL -lassimp -lpthread
INCLUDES = -I/usr/include/ClanLib-3.0
CFLAGS = -Wno-deprecated -std=c++11
CC = clang++ 
EXE = main.out

HLIST = $(shell ls *.h)
OBJECTS = $(HLIST:.h=.o)

main: $(OBJECTS) driver.cpp
	$(CC) -o $(EXE) $(OBJECTS) driver.cpp $(LIBS) $(INCLUDES) $(CFLAGS)
%.o: %.cpp %.h
	$(CC) $(CFLAGS) $(INCLUDES) -c $<
	

clean:
	rm main.out && rm *.o


