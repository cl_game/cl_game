#ifndef GBUFFER_H
#define GBUFFER_H

#include <ClanLib/core.h>
#include <ClanLib/Display/Render/texture_2d.h>
#include <array>

using namespace clan;


class GBuffer {

    public:
    
        GBuffer( GraphicContext &gc );
        void BindForReading( GraphicContext &gc );
        void BindForWriting( GraphicContext &gc );
        Texture2D * GetBufferTexture( int num );
    
    private:

        FrameBuffer fb;
        std::array<Texture2D,4> targets;


};


#endif
